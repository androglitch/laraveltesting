<?php

namespace App\Classes;

use Exception;

class TestClass {

	private $_id;
	private $_stringId;

	public function __construct(){
		$this->_id = 10; //rand(0, 10);
		$this->_stringId = (string) $this->_id;
	}

	public function sayHelloWorld(){
		return "Hello World";
	}

	public function getId(){
		return $this->_id;
	}

	public function generateNewId(){
		return rand(0, 10);
	}

	public function isCustomClass(){
		return true;
	}

	public function giveMeAFalseBoolean(){
		return false;
	}

	public function jsonReturn(){
		$array = [
			'id' => 1,
			'name' => 'Pepe'
		];

		return json_encode($array);
	}

	public function createArray($positions = 10){
		$array = array_fill(0, $positions, 0);
		return $array;
	}

	public function throwException(){
		throw new Exception("Exception");
	}

	public function _funct(){
		$i = 1;
	}
}