<?php

namespace App\Classes;

class Reservation
{
    public $tickets;
    public $email;

    public function __construct($tickets, $email)
    {
        $this->tickets = $tickets;
        $this->email = $email;
    }

    public function complete($paymentGateway, $paymentToken)
    {
        $charge = $paymentGateway->charge($this->totalPrice(), $paymentToken);
        return Order::forReservation($this, $charge);
    }

    private function totalPrice()
    {
        return $this->tickets->sum('price');
    }
}