<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PrintEnvVariable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'print:env {variable}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $variable = $this->argument('variable');
        echo env($variable);
        return 0;
    }
}
