<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PrintConfigVariable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'print:config {variable}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $variable = $this->argument('variable');
        $connection = config('database.default');
        $driver = config("database.connections.{$connection}.database");
        echo $driver;
        return 0;
    }
}
