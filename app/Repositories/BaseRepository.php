<?php

namespace App\Repositories;

use Validator;
use App\Exceptions\CustomValidationException;

abstract class BaseRepository
{
    protected $_query;
    protected $_count;
    
    protected $_data;
    protected $_isNew;
    protected $_id;
    protected $_newRegister;

    protected $_limit = null;
    protected $_page = null;
    protected $_orderBy = [
        'column' => 'created_at',
        'direction' => 'desc',
    ];
    protected $_multiSort = null;
    protected $_with = [];
    
    protected $_where = [];
    protected $_whereIn = [];
    protected $_whereRaw = [];
    protected $_having = [];

	public function find($id){
        return $this->model::where('id',$id)->first();
    }

    public function findBy($where){
        return $this->model::where($where)->first();
    }

    public function all($orderBy = null, $direction = null){
        if($orderBy!=null){
            $direction=$direction!=null?$direction:'asc';
            return $this->model::orderBy($orderBy, $direction)->get();    
        }
        return $this->model::get();
    }

    public function store($id = null, array $data = null, $attachments = null){
        $this->_initStore($id, $data);
        if(method_exists($this, '_beforeValidate'))
            $this->_beforeValidate();
        $this->_validate();

        if(method_exists($this, '_beforeSave'))
            $this->_beforeSave();
        
        $this->_newRegister = $this->model::updateOrCreate(['id' => $this->_id], $this->_data);
        
        if(method_exists($this, '_afterSave'))
            $this->_afterSave();

    	return $this->_newRegister;
    }

    public function delete($id){
        $this->_id = $id;
        $exists=$this->find($id) != null;
        
        if(method_exists($this, '_beforeDelete'))
            $this->_beforeDelete();
    	$this->model::where('id',$id)->delete();
        if(method_exists($this, '_afterDelete'))
            $this->_afterDelete();

        return $exists;
    }

    public function filter($input, $orderBy, $limit = null, $page = null, $multiSort = false){
        $this->_orderBy = $orderBy;
        $this->_limit = $limit;
        $this->_page = $page;
        $this->_multiSort = $multiSort;

        $this->_createWhereConditions($input);
        $this->_makeQuery();

    	$response=[
    		'data' => $this->_query->get(),
    		'count' => $this->_query->count(),
            'total' => $this->model::count()
    	];
    	return $response;
    }

    protected function _validate(){
        if(count($this->_rules) == 0)
            return;
        $validator=Validator::make($this->_data, $this->_rules, $this->_messages);
        if($validator->fails()){
            $stringErrors="Se encontraron los siguientes errores:<br />";
            $index=1;
            foreach($validator->errors()->all() as $e){
                $stringErrors.=($index++)."- ".$e."<br />";
            }
            $stringErrors.="Por favor, corrígelos para poder guardar el registro.";
            throw new CustomValidationException($stringErrors);
        }
    }

    protected function _initStore($id, $data){
        $this->_data = $data;
        $this->_id = $id;
        $this->_isNew = ($id == null);
    }

    protected function _makeQuery(){
        $this->_query = $this->model::where($this->_where);
        $this->_count = $this->model::where($this->_where);

        $this->_addPagination();
        $this->_addOrder();
        $this->_addRelations();
        $this->_addMultiSort();
        $this->_addWhereIn();
        $this->_addWhereRaw();
        $this->_addHaving();
    }

    protected function _createWhereConditions($input){
        if(is_array($input) && count($input) > 0){
            foreach($input as $key=>$value){
                if($value=='' || $value==null) continue;
                $this->_where[]=[$key,'like','%'.$value.'%'];
            }
        }
    }

    protected function _addPagination(){
        if($this->_limit == null || $this->_page == null)
            return ;
        $this->_query->skip($this->_limit * ($this->_page - 1))->limit($this->_limit);
    }

    protected function _addOrder(){
        if($this->_orderBy == null || !isset($this->_orderBy['column']) || !isset($this->_orderBy['direction']))
            return ;
        $this->_query->orderBy($this->_orderBy['column'],$this->_orderBy['direction']);
    }

    protected function _addRelations(){
        if(!isset($this->_with) || !is_array($this->_with) || count($this->_with) == 0)
            return ;
        $this->_query->with($this->_with);
    }

    protected function _addMultiSort(){
        if($this->_multiSort == null)
            return ;
        foreach ($this->_multiSort as $s) {
            $ob=json_decode($s,true);
            $asc=$ob['ascending']?'asc':'desc';
            $this->_query->orderBy($ob['column'],$asc);
        }
    }

    protected function _addWhereIn(){
        if(count($this->_whereIn) == 0)
            return ;
        foreach($this->_whereIn as $key=>$value){
            $this->_query->whereIn($key,$value);
            $this->_count->whereIn($key,$value);
        }
    }

    protected function _addWhereRaw(){
        if(count($this->_whereRaw) == 0)
            return ;
        foreach($this->_whereRaw as $raw){
            $this->_query->whereRaw($raw);
            $this->_count->whereRaw($raw);
        }
    }

    protected function _addHaving(){
        if(count($this->_having) == 0)
            return ;
        foreach($this->_having as $key=>$val){
            $this->_query->having($key,$val);
            $this->_count->having($key,$val);
        }
    }
}