<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Rule;
use Hash;

class UserRepository extends BaseRepository{

	protected $_rules = [
		'name' => 'required',
        'surname' => 'required',
		'email' => 'exclude_unless:id,null|required|email|unique:users',
		'password' => 'exclude_unless:id,null|required|string|min:8'
	];
	protected $_messages = [
		'name.required' => 'El nombre es obligatorio',
		'surname.required' => 'Los apellidos es obligatorio',
		'id_role.required' => 'El pefil es obligatorio',
		'email.required' => 'El correo electrónico es obligatorio',
		'email.email' => 'El correo electrónico no es válido',
		'email.unique' => 'El correo electrónico especificado ya está siendo usado por otro comercial',
		'password.required' => 'La contraseña es obligatoria',
		'password.min' => 'La contraseña debe tener al menos :min caracteres',
	];

    public function __construct(User $model){
        $this->model = $model;
    }

    /*public function _beforeSave(){
    	if(isset($this->_data['password']))
    		$this->_data['password'] = Hash::make($this->_data['password']);
    }*/

    public function calculate(){
    	return $this->model->calculateSomething();
    }

}