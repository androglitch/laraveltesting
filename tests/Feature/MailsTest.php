<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;
/**
 * 
 * @group mailing
 * 
 * */

class MailsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testMailable()
    {
        Mail::fake();

        Mail::to('ja.espinosa.santacruz@gmail.com')->send(new OrderShipped);
        Mail::assertSent(OrderShipped::class);
                
    }

    public function testNotification(){
        $this->travel(5)->years();
        $this->assertTrue(true);
    }
}
