<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Repositories\UserRepository;
use App\Models\User;

/**
 * 
 * @group repositories
 * 
 * */

class UsersRepositoryTest extends TestCase
{
    /**
     * A basic feature test example.
     * @return void
     */

    use RefreshDatabase;

    public function testCreation()
    {
        $model = new User();
        $factory = User::factory(1)->make()->toArray();
        $repository = new UserRepository($model);
        $newRegister = $repository->store(null, $factory[0]);
        $this->assertModelExists($newRegister);
    }

    public function testMockingCreation(){
        $model = new User();
        $mock = $this->mock(User::class);
        $mock->shouldReceive('calculateSomething')->andReturn(4)->once();

        $mockResult = $mock->calculateSomething();
        $modelResult = $model->calculateSomething();

        $this->assertSame($modelResult, $mockResult);
    }

    public function testSpyCreation(){
        $model = new User();
        $spy = $this->spy(User::class);
        $factory = User::factory(1)->make()->toArray();
    
        $repository = new UserRepository($spy);
        $newRegister = $repository->store(null, $factory[0]);

        $spy->shouldHaveReceived('updateOrCreate')
            ->with(['id' => null], $factory[0]);
    }

    public function testMockingMethod(){
        $model = new User();
        $mock = $this->createMock(User::class);
        $repository = new UserRepository($mock);
        $mock->expects($this->exactly(2))->method('calculateSomething')->willReturn(4);
        $mockResult = $mock->calculateSomething();
        $modelResult = $repository->calculate();
        $this->assertSame($modelResult, $mockResult);
    }
}



