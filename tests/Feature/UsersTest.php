<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Tweet;
use Hash;

/**
 * 
 * @group repositories
 * @group users
 * 
 * */

class UsersTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUsersCreation()
    {
        $result = User::factory(50)->make();
        $this->assertCount(50, $result);
    }

    public function testUserFullName()
    {
        $result = User::factory(1)->create([
            'name' => 'George',
            'surname' => 'Orwell'
        ]);
        $user = User::first();
        $this->assertSame('george Orwell', $user->full_name);
    }

    public function testUserTweetCreation(){
        $result = User::factory(1)->hasTweets(3, [
            'content' => 'ASDF'
        ])->create();
        $tweets = Tweet::whereUserId($result[0]['id'])->get();
        $this->assertCount(3, $tweets);
    }

    public function testEncryptMutatorPassword(){
        $result = User::factory(1)->create([
            'name' => 'George',
            'surname' => 'Orwell',
            'password' => 'secret'
        ]);
        $user = User::find($result[0]['id']);
        $check = Hash::check('secret', $user->password);
        $this->assertTrue($check);
    }
    public function testEncryptMutatorWithHashedPassword(){
        $result = User::factory(1)->create([
            'name' => 'George',
            'surname' => 'Orwell',
            'password' => Hash::make('secret')
        ]);
        $user = User::find($result[0]['id']);
        $check = Hash::check('secret', $user->password);
        $this->assertTrue($check);
    }

    public function testTweetFunctionUser(){
        $user = User::factory()->create([
            'name' => 'George',
            'surname' => 'Orwell',
            'password' => Hash::make('secret')
        ]);
        $result = $user->calculateSomething();
        $this->assertNotSame(2, $result);
    }
}