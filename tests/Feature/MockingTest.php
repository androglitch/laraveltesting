<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Classes\TestClass;
use App\Repositories\UserRepository;
use App\Http\Controllers\API\FollowingController;
use App\Http\Controllers\API\TweetController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Http;
use App\Mail\OrderMail;
use Mockery;
use Mockery\MockInterface;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * 
 * @group mocks
 * 
 * */
class MokingTest extends TestCase
{
    use WithoutMiddleware;

    private $sayHelloWorldReturn = "Hello World";
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    
    public function _testInstanceController(){
        $this->instance(
            TweetController::class,
            Mockery::mock(TweetController::class, function (MockInterface $mock) {
                $mock->shouldReceive('calculateSomething')->andReturn(5)->once();
            })
        );
        $response = $this->get('/api/tweets/test');
        $response->assertExactJson(['response' => "Hello Worlds"]);
    }

    public function __testInstance(){

        Http::fake([
            'http://127.0.0.1:8000/api/tweets/test' => Http::response(['response' => "Hello Worlds", 'success' => true], 200, ['Content-type' => 'application/json'])
        ]);
        $response = $this->get('/api/tweets/test');
        $response->assertExactJson(['success' => true,'response' => "Hello Worlds"]);
        //$response = Http::get('http://127.0.0.1:8000/api/tweets/test');
        //$this->assertEquals(['success' => true,'response' => "Hello Worlds"], $response->json());

    }

    public function _testInstance(){
        $this->instance(
            User::class,
            Mockery::mock(User::class, function (MockInterface $mock) {
                $mock->shouldReceive('calculateSomething')->andReturn(5)->once();
            })
        );
        $repo = new UserRepository(new User);
        $result = $repo->calculate();
        $this->assertSame(4, $result);
    }

    public function _testSpies(){
        $model = new User();
        $spy = $this->spy(User::class);
        $factory = User::factory(1)->make()->toArray();
    
        $repository = new UserRepository($spy);
        $newRegister = $repository->store(null, $factory[0]);

        $spy->shouldHaveReceived('updateOrCreate')
            ->with(['id' => null], $factory[0])->once();

        $this->assertInstanceOf(User::class, $newRegister);
    }


    /*public function testMockMail(){
        Mail::fake();
        $user = User::first();
        $response = $this->actingAs($user)->get('/api/tweets');
        Mail::assertSent(OrderMail::class);
        Mail::assertNothingSent();
        Mail::assertSent(OrderMail::class, 1);
        Mail::assertNotSent(OrderMail::class);
    }

    public function testMockNotification(){
        Notification::fake();

        Notification::assertSentTo([
            $usuarios
        ], OrderNotification::class);
        Notification::assertNotSentTo([
            $usuarios
        ], OrderNotification::class);
        Notification::assertCount(4);
    }

    public function testMockStorage(){
        Storage::fake('local');
        Storage::disk('local')->assertExists('photo1.jpg');
        Storage::disk('local')->assertExists(['photo1.jpg', 'photo2.jpg']);
        Storage::disk('local')->assertMissing('missing.jpg');
        Storage::disk('local')->assertMissing(['missing.jpg', 'non-existing.jpg']);
        Storage::disk('local')->assertDirectoryEmpty('/wallpapers');
    }

    public function testQueuStorage(){
        Queue::fake();
        Queue::assertPushedOn('queue', OrderJob::class);
        Queue::assertPushed(OrderJob::class, 2);
        Queue::assertNotPushed(OrderJob::class);
    }*/

}







