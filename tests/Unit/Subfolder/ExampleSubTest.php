<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Classes\TestClass;
use App\Classes\Reservation;
use Exception;
use Mockery;

class ExampleSubTest extends TestCase
{

    //https://phpunit.readthedocs.io/es/latest/writing-tests-for-phpunit.html

    private $_id;
    private $_class;

    protected function setUp(): void {
        parent::setUp();
        $this->_id = 1;
        $this->_class = new TestClass();

    }

    protected function tearDown(): void {
        unset($this->_id);
    }

    public function test_the_reservation_is_completed_successfully()
    {
        $tickets = collect([
            ['price' => 1250],
            ['price' => 1250],
            ['price' => 1250],
        ]);

        $paymentGateway = Mockery::spy('PaymentGateway');
        

        $reservation = new Reservation($tickets, 'adam@example.com');
        $order = $reservation->complete($paymentGateway, 'tok_valid-token');

        $this->assertSame(1, $order);
        $paymentGateway->shouldHaveReceived('charge')->with(3750, 'tok_valid-token')->once();
    }

    /**
     * @covers TestClass::_funct
    * */

    public function testIdContainedIn() {
        $this->assertContains(1, [1, 2, 3, 4, 10]);
        
    }

    /**
     * @group specification
     * @test
     */
    public function IfIsTrue() {
        $this->assertTrue(true);
    }

    /**
     * @author jose
     * */

    public function testIfIsFalse() {
        $this->assertFalse(false);
    }

    public function testIfIsNotTrue() {
        $this->assertNotTrue(false);
    }

    public function testIfIsNotFalse() {
        $this->assertNotFalse(true);
    }

    /**
    * Same comprueba el tipo
    */

    public function testIfIsTheSame(){
        $this->assertSame(1, $this->_id);
    }

    /**
    * Equals NO comprueba el tipo
    */

    public function testIfIsEquals(){
        $this->assertEquals('1', $this->_id);
    }

    public function testIfContains(){
        $this->assertContains(1, [1, 2, 3, 4]);
    }

    public function testInsanceOf(){
        $this->assertInstanceOf(TestClass::class, $this->_class);
    }

    /**
     * Aclaración sobre internal type
     * */

    public function _testInternalType(){
        $this->assertIsArray();
        $this->assertIsNotArray();
        $this->assertIsBool();
        $this->assertIsNotBool();
        $this->assertIsCallable();
        $this->assertIsNotCallable();
        $this->assertIsFloat();
        $this->assertIsNotFloat();
        $this->assertIsInt();
        $this->assertIsNotInt();
        $this->assertIsIterable();
        $this->assertIsNotIterable();
        $this->assertIsNumeric();
        $this->assertIsNotNumeric();
        $this->assertIsObject();
        $this->assertIsNotObject();
        $this->assertIsResource();
        $this->assertIsNotResource();
        $this->assertIsScalar();
        $this->assertIsNotScalar();
        $this->assertIsString();
        $this->assertIsNotString();
        $this->assertIsReadable();
        $this->assertIsNotReadable();
        $this->assertIsWritable();
        $this->assertIsNotWritable();
    }

    public function testContainsOnly(){
        $aux = [
            new TestClass(),
            new TestClass(),
            new TestClass(),
            new TestClass()
        ];
        $this->assertContainsOnly(TestClass::class, $aux);
    }

    public function _testXML(){
        $this->assertEqualXMLStructure();
    }

    public function _testJSONFromFile(){
        $this->assertJsonFileEqualsJsonFile();
    }

    public function testJSONFromString(){
        $json = $this->_class->jsonReturn();
        $jsonExpected = '{
            "id": 1,
            "name": "Pepe"
        }';
        $this->assertJsonStringEqualsJsonString($jsonExpected, $json);
    }

    public function _testNumbers(){
        $this->assertLessThan();
        $this->assertLessOrEqualThan();
        $this->assertGreaterThan();
        $this->assertGreaterOrEqualThan();
        $this->assertNan();
        $this->assertIfNumeric();
    }

    public function testException(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Exception");
        $this->_class->throwException();
    }
    
    public function testOutputString(){
        $this->expectOutputString("Hello World");
        echo $this->_class->sayHelloWorld();
    }
}
