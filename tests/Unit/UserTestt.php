<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Assert;
use App\Classes\TestClass;
use Exception;

class UserTest extends TestCase
{

    private $_id;
    private $_class;


    protected function setUp(): void{
        parent::setUp();
        $this->_class = new TestClass();
        $this->_id = 1;
    }

    protected function tearDown(): void{
        unset($this->_id);
        unset($this->_class);
    }
    
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCustomClass()
    {
        $isCustomClass = $this->_class->isCustomClass();
        //Assert::assertTrue($this->_id == 1);
        $this->assertTrue($isCustomClass);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testFalseBoolean()
    {
        $false = $this->_class->giveMeAFalseBoolean();
        //sleep(10);
        //Assert::assertTrue($this->_id == 1);
        $this->assertFalse($false);
    }

    public function testIdIsTen()
    {
        $id = $this->_class->getId();
        $this->assertEquals(10, $id);
    }

    public function testIdIsTenInt()
    {
        $id = $this->_class->getId();
        $this->assertSame(10, $id);
    }

    public function testIdContainedIn() {
        $id = $this->_class->getId();
        $this->assertContains($this->_id, [1, 2, 3, 4, 10]);
    }

    public function testClassIsInstanceOfTestClass(){
        $this->assertInstanceOf(TestClass::class, $this->_class);
    }

    public function testIdIsIntType(){
        $id = $this->_class->getId();
        $this->assertIsInt($id);
    }

    public function testArrayGeneration(){
        $array = $this->_class->createArray();
        $this->assertCount(10, $array);
    }

    public function testJsonString(){
        $json = $this->_class->jsonReturn();
        $jsonExpected = '{
            "id": 1,
            "name": "Pepe"
        }';
        $this->assertJsonStringEqualsJsonString($jsonExpected, $json);
    }

    public function testNewIdGeneration(){
        $id = $this->_class->generateNewId();
        $this->assertLessThanOrEqual(10, $id);
        $this->assertGreaterThanOrEqual(0, $id);
    }

    public function testHelloWorld(){
        $helloWorld = $this->_class->sayHelloWorld();
        $this->expectOutputString("Hello World");
        print($helloWorld);
    }

    public function testException(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Excepción");
        $this->_class->throwException();
    }
}